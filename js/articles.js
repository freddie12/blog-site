window.onload = displayResult();
var xml;
var xsl;

function displayResult()
{
//load xml and xsl
    xml = loadXMLDoc("../xml/blog.xml");
    xsl = loadXMLDoc("../xsl/articles.xsl");
// code for IE
    if (window.ActiveXObject || xhttp.responseType == "msxml-document")
    {
      //combine xml with xsl
        ex = xml.transformNode(xsl);
        document.getElementById("content").innerHTML = ex;
    }
// code for Chrome, Firefox, Opera, etc.
    else if (document.implementation && document.implementation.createDocument)
    {
        xsltProcessor = new XSLTProcessor();
      //combine xml with xsl
        xsltProcessor.importStylesheet(xsl);
        resultDocument = xsltProcessor.transformToFragment(xml, document);
        document.getElementById("content").appendChild(resultDocument);
    }
}







