var doc = $(document);
var displayed = false;
var contact = $('#new-article-form');
var children = $('#new-article-form *:not(.different)');
var allChildren = $('#new-article-form *');
var contactFormHeight = 580;


doc.ready( function() {
    contact.css({height:0});
});


// when selecting the tet field it removes the default text
function validateFormFeild(defaultText, id, textarea) {
    var current = getNewArticleFormDetails();//return text inside form
    var element;

    if(!textarea) element = '#'+id;
    else element          = 'textarea#'+id;

    if(current[id]==defaultText) $(element).val('');
    else if(current[id]=='') $(element).val(defaultText);
    else {}
}



function getNewArticleFormDetails() {
    var details = new Array();
    details['blogTitle']    = $("input#blogTitle").val();
    details['blogSubTitle']     = $("input#blogSubTitle").val();
    details['blogBody']         = $("textarea#blogBody").val();
    details['search']       = $("#search").val();

    return details;
}


function displayNewArticleForm(speed, animation) {
    if(!displayed) {
        window.scrollTo(0, 0);
        //height is the height of the div
        contact.css({display: 'block'}).animate({height:contactFormHeight}, speed, animation);
        children.fadeIn(1100);
        displayed = true;
    } else {
        removeNewArticleForm(1300, 'easeOutQuint');
    }
}

function removeNewArticleForm(speed, animation) {
    if(displayed) {
        allChildren.fadeOut(1100);
        contact.delay(200).animate({height:0}, speed, animation);
        displayed = false;
    } else {
        displayNewArticleForm();
    }
}