window.onload = displayResult();
var xml;
var xsl;

function displayResult()
{
//load xml and xsl
    xml = loadXMLDoc("xml/blog.xml");
    xsl = loadXMLDoc("xsl/main.xsl");
// code for IE
    if (window.ActiveXObject || xhttp.responseType == "msxml-document")
    {

      //combine xml with xsl
        ex = xml.transformNode(xsl);
        document.getElementById("content").innerHTML = ex;
    }
// code for Chrome, Firefox, Opera, etc.
    else if (document.implementation && document.implementation.createDocument)
    {
        xsltProcessor = new XSLTProcessor();
      //combine xml with xsl
        xsltProcessor.importStylesheet(xsl);
        resultDocument = xsltProcessor.transformToFragment(xml, document);
        document.getElementById("content").appendChild(resultDocument);
    }
}


//----------------------------
//save new article file to xml : written for future development
//----------------------------

function saveArticle() {
    var data = getNewArticleFormInput();

    var ok = true;

    titleWarning = $('#contact-email-warning')
    subTitleWarning = $('#contact-subject-warning');
    bodyWarning = $('#contact-body-warning');


    if(data['blogTitle'] == '' || data['blogTitle'] == 'Title') {
        displayMessage('Please fill title');
        ok = false;
    } else if(data['body'] == '' || data['body'] == 'Blog body..') {
        displayMessage('Please fill body');
        ok = false;
    } else {}

    if(ok) {
        data = "email="+data['email']+"&subject="+data['subject']+"&body="+data['body'];
        $.ajax({
            type: "POST",
            url: "../ajaxFunctions/sendMessage.php",
            data: data,
            success: function(res) {
                if (res == 1) {
                    resetContactForm();
                    removeContact(1300, 'easeOutQuint');
                    displayMessage('Message Sent!');
                } else {
                    displayMessage('Message not sent. Try again');
                }
            }

        });
    } else {
        displayMessage('Please fill all fields');
    }


}

//---------------------
// get the form details
//---------------------

function getNewArticleFormInput() {
    var details = new Array();
    details['blogTitle']    = $("input#blogTitle").val();
    details['blogSubTitle']     = $("input#blogSubTitle").val();
    details['blogBody']         = $("textarea#blogBody").val();
    details['search']       = $("#search").val();

    return details;
}




