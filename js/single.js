//read in xml file
xml = loadXMLDoc("../xml/blog.xml");
xsl = loadXMLDoc("../xsl/single.xsl");

//get the id
id = getId();
//get nodes and write temp xml
article = xml.getElementsByTagName("article")[id];
//if image doesn't exist then leave blank
var img;
if(article.childNodes[9].hasChildNodes())
    img = article.childNodes[9].firstChild.nodeValue;
else
    img = "";

xmlString =
    '<?xml version="1.0" encoding="UTF-8"?>' +
    '<blog>' +
        '<article>' + '' +
            '<title>' + article.childNodes[1].firstChild.nodeValue + '</title>' +
            '<description>' + article.childNodes[3].firstChild.nodeValue + '</description>' +
            '<date>' + article.childNodes[5].firstChild.nodeValue + '</date>' +
            '<time>' + article.childNodes[7].firstChild.nodeValue + '</time>' +
            '<image>' +  img  + '</image>' +
            '<body>' + article.childNodes[11].firstChild.nodeValue + '</body>' +
        '</article>' +
    '</blog>';

//convert string to temp xml file
convertedStringToXml = stringtoXML(xmlString);

// code for IE
    if (window.ActiveXObject || xhttp.responseType == "msxml-document")
    {
        //attach xsl to xsl
        ex = convertedStringToXml.transformNode(xsl);
        document.getElementById("content").innerHTML = ex;
    }
// code for Chrome, Firefox, Opera, etc.
    else if (document.implementation && document.implementation.createDocument)
    {
        xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        resultDocument = xsltProcessor.transformToFragment(convertedStringToXml, document);
        document.getElementById("content").appendChild(resultDocument);
    }


// takes the url query and seperates the id ' ?id=1 '
function getId() {
    var result = "Not found",
        tmp = [];
    // take query string and split it
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        //      split each property = value
        tmp = items[index].split("=");
        // take the value which is the id
        result = decodeURIComponent(tmp[1]);
    }
    return result;
}