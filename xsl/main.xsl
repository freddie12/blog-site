<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <div id="full-container">

            <!-- this is a form to add new articles-->
            <div id="new-article-form">
                <div class="wrapper">
                    <div onclick="removeNewArticleForm(1300, 'easeOutQuint')" class="right white close back-black">
                        X
                    </div>
                    <h1 class="white">
                        New Article
                    </h1>
                    <div class="full-width clear">
                        <form id="new-article-form-child" action="" method="post">
                            <div class="left full-width">
                                <input type="text" onblur="validateFormFeild('Title', 'blogTitle', false)" onfocus="validateFormFeild('Title', 'blogTitle', false)" name="blogTitle" id="blogTitle" value="Title"/>
                                <input type="text" onblur="validateFormFeild('Subtitle', 'blogSubTitle', false)" onfocus="validateFormFeild('Subtitle', 'blogSubTitle', false)" name="blogSubTitle" id="blogSubTitle" value="Subtitle"/>
                                <textarea form="new-article-form-child" onblur="validateFormFeild('Blog body..', 'blogBody', true)" onfocus="validateFormFeild('Blog body..', 'blogBody', true)" name="blogBody" id="blogBody">Blog body..</textarea>
                            </div>

                            <div class="left clear full-width">
                                <a href="javascript:;">
                                    <div id="new-article-form-button" onclick="saveArticle()">
                                        <h2 class="white">Save!</h2>
                                    </div>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end new article form-->

            <!--start user nav-->
            <div id="nav-container" class="navbar">
                <div class="uppernav">
                </div>
                <div class="wrapper">
                    <!--LOGO-->
                    <div id="logo" class="white left">
                        <a href="/"><h1 class="white">iBlog</h1></a>
                    </div>
                    <!--LOGO END-->

                    <div class="left">
                        <ul class="list">
                            <li class="left">  <a href="articles/">articles</a></li>
                            <li class="left">  <a href="javascript:;" onclick="displayNewArticleForm(1000, 'easeOutQuint')">new</a></li>
                        </ul>
                    </div>
                </div>
                <!--end user nav-->
            </div>
            <!--end nav container-->

            <!--start main body container-->
            <div class="body-container">
                <div class="wrapper">
                    <div id="main-body">

                        <!--LATEST ENTRY-->
                        <div id="latestEntry" class="horizontal-divide background-white-box left">
                            <h1>
                                <xsl:value-of select="blog/article[last()]/title"/>
                            </h1>
                            <h4>
                                <xsl:value-of select="blog/article[last()]/description"/>
                            </h4>
                            <h6>
                                <xsl:value-of select="blog/article[last()]/date"/>
                            </h6>
                            <img class="default" align="right">
                                <xsl:attribute name="src">
                                    <xsl:value-of select="blog/article[last()]/image"/>
                                </xsl:attribute>
                            </img>
                            <p class="black">
                                <xsl:value-of select="blog/article[last()]/body"/>
                            </p>
                        </div>

                        <!--left hand welcome-->
                        <div id="welcomeBoxHolder" class="holder horizontal-divide clear left">
                            <div id="welcomeBox" class="background-white-box">
                                <h1>
                                    Welcome
                                </h1>
                                <p class="black">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                </p>
                            </div>

                            <div class="vertical-divide"/>

                            <div id="latestThreeBox" class="background-white-box">
                                <h1>
                                    Latest
                                </h1>
                                <br/>

                                <div>
                                    <xsl:variable name="link"  />
                                    <a class="black">
                                        <xsl:attribute name="href">
                                            single/?id=<xsl:value-of select="blog/article[last()-1]/@id"/>
                                        </xsl:attribute>
                                        <h3>
                                            <xsl:value-of select="blog/article[last()-1]/title"/>
                                        </h3>
                                    </a>
                                    <h6>
                                        <xsl:value-of select="blog/article[last()-1]/date"/>
                                    </h6>
                                    <p class="black">
                                        <xsl:variable name="subbody" select="blog/article[last()-1]/body" />
                                        <xsl:value-of select="substring($subbody, 1, 20)"/>
                                        <a>
                                            <xsl:attribute name="href">
                                                single/?id=<xsl:value-of select="blog/article[last()-1]/@id"/>
                                            </xsl:attribute>
                                            <span class="red"> ....Read More</span>
                                        </a>
                                    </p>
                                </div>

                                <br/>

                                <div>
                                    <a class="black">
                                        <xsl:attribute name="href">
                                            single/?id=<xsl:value-of select="blog/article[last()-2]/@id"/>
                                        </xsl:attribute>
                                        <h3>
                                            <xsl:value-of select="blog/article[last()-2]/title"/>
                                        </h3>
                                    </a>

                                    <h6>
                                        <xsl:value-of select="blog/article[last()-2]/date"/>
                                    </h6>
                                    <p class="black">
                                        <xsl:variable name="subbody" select="blog/article[last()-2]/body" />
                                        <xsl:value-of select="substring($subbody, 1, 20)"/>
                                        <a>
                                            <xsl:attribute name="href">
                                                single/?id=<xsl:value-of select="blog/article[last()-2]/@id"/>
                                            </xsl:attribute>
                                            <span class="red"> ....Read More</span>
                                        </a>
                                    </p>
                                </div>

                                <br/>

                                <div>
                                    <a class="black">
                                        <xsl:attribute name="href">
                                            single/?id=<xsl:value-of select="blog/article[last()-3]/@id"/>
                                        </xsl:attribute>
                                        <h3>
                                            <xsl:value-of select="blog/article[last()-3]/title"/>
                                        </h3>
                                    </a>
                                    <h6>
                                        <xsl:value-of select="blog/article[last()-3]/date"/>
                                    </h6>
                                    <p class="black">
                                        <xsl:variable name="subbody" select="blog/article[last()-3]/body" />
                                        <xsl:value-of select="substring($subbody, 1, 20)"/>
                                        <a>
                                            <xsl:attribute name="href">
                                                single/?id=<xsl:value-of select="blog/article[last()-3]/@id"/>
                                            </xsl:attribute>
                                            <span class="red"> ....Read More</span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---end main body-->

            <!--start footer-->
            <div id="footer">
                <img class="blogman" src="../image/image/blogman.png"/>
            </div>
            <!--end footer-->

        </div>
        <!--end full container-->

    </xsl:template>
</xsl:stylesheet>