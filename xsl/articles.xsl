<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <div id="full-container">

            <!-- this is a form to add new articles-->
            <div id="new-article-form">

                <div class="wrapper">
                    <div onclick="removeNewArticleForm(1300, 'easeOutQuint')" class="right white close back-black">
                        X
                    </div>
                    <h1 class="white">
                        New Article
                    </h1>
                    <div class="full-width clear">
                        <form id="new-article-form-child" action="" method="post">
                        <div class="left full-width">
                            <input type="text" onblur="validateFormFeild('Title', 'blogTitle', false)" onfocus="validateFormFeild('Title', 'blogTitle', false)" name="blogTitle" id="blogTitle" value="Title"/>
                            <div class="clear">
                                <input type="text" onblur="validateFormFeild('Subtitle', 'blogSubTitle', false)" onfocus="validateFormFeild('Subtitle', 'blogSubTitle', false)" name="blogSubTitle" id="blogSubTitle" value="Subtitle"/>
                            </div>
                            <textarea form="new-article-form-child" onblur="validateFormFeild('Blog body..', 'blogBody', true)" onfocus="validateFormFeild('Blog body..', 'blogBody', true)" name="blogBody" id="blogBody">Blog body..</textarea>
                        </div>

                            <div class="left clear full-width">
                                <a href="javascript:;">
                                    <div id="new-article-form-button" onclick="saveArticle()">
                                        <h2 class="white">Save!</h2>
                                    </div>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--user nav start-->
            <div id="nav-container" class="navbar">
                <div class="uppernav">
                </div>
                <div class="wrapper">
                    <!--LOGO-->
                    <div id="logo" class="white left">
                        <a href="/"><h1 class="white">iBlog</h1></a>
                    </div>
                    <!--LOGO END-->

                    <div class="left">
                        <ul class="list">
                            <li class="left">  <a href="/articles">articles</a></li>
                            <li class="left">  <a href="javascript:;" onclick="displayNewArticleForm(1000, 'easeOutQuint')">new</a></li>
                        </ul>
                    </div>
            </div>
        </div>


        <div class="body-container">
            <div class="wrapper">
                <div id="main-body">

                    <!-- display each article-->
                    <xsl:for-each select="blog/article">
                        <xsl:sort select="position()" data-type="number" order="descending"/>
                        <a>
                            <xsl:attribute name="href">
                                /single/?id=<xsl:value-of select="@id"/>
                            </xsl:attribute>
                            <div class="background-white-box clear horizontal-divide full-width">
                                <h2>
                                    <xsl:value-of select="title"/>
                                </h2>
                                <h4>
                                    <xsl:value-of select="description"/>
                                </h4>
                                <h6>
                                    <xsl:value-of select="date"/>
                                </h6>
                                <!-- IN ORDER TO GET THE FADED EFFECT
                                     USING SPRITE POSISTIONING ON EACH
                                     ARTICLE IMAGE, THE HTML MUST HAVE
                                     INNER STYLING TO SET THE BACKGROUND
                                     URL AND POSITION-->
                                <div class="imageSection">
                                    <xsl:attribute name="style">background:
                                        <xsl:choose>
                                            <xsl:when test="image != ''">
                                                url('<xsl:value-of select="image"/>') -40px -40px;
                                            </xsl:when>
                                            <xsl:otherwise>
                                                url('../image/image/default.png') -40px -40px;
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                </div>
                                <div class="fader-cover"/>
                            </div>
                        </a>
                    </xsl:for-each>
                </div>
            </div>
        </div>

        <!--footer goes in here.-->

        <div id="footer">
            <img class="blogman" src="../image/image/blogman.png"/>
        </div>


    </div>
    </xsl:template>
</xsl:stylesheet>